#include <iostream>		// Include all needed libraries here
#include <fstream>
#include <wiringPi.h>
#include <cmath>
#include "servoControl.h"
#include "FPDBlock.h"
#include "fcontrol.h"
#include "IPlot.h"


using namespace std;
int dutty;
//encoder
#define channelPinA  27
#define channelPinB  28
int counter0 = 0, counter1 = 0;
int contEnA, contEnB;
volatile int ISRCounter = 0;
double enc2rad = 2.0 * 3.14159 / 5880.0; //28*210=5880 pulse/rev.

//control
double t0 = 0, t1 = 0, dt = 0;
int cp0 = 0, cp1 = 0, dcp = 0;
double v0 = 0, v1 = 0, p0 = 0, p1 = 0;
double dts = 0.004; //seconds
//double dts = 0.01;
//control variables
//PIDBlock Cv(29.926, 1121.6, 0.0, dts);
PIDBlock Cv(207.7, 11927.9, 0.0, dts);
FPDBlock Fcv(323.98, 7388.5, -0.91, dts);
double csv = 0, esv = 0;
SamplingTime ts(dts);


void doEncodeA()
{
  contEnA ++;
  if (digitalRead(channelPinA) == digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
}
void doEncodeB() {
  contEnB ++;
  if (digitalRead(channelPinA) != digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
}
double getPos()
{
  //pos in rad
  return double(ISRCounter) * enc2rad;

}
double getVel()
{
  t0 = t1; //last time is the old current time
  t1 = micros();
  dt = (t1 - t0); //in [us];
  cp0 = cp1; //last pos is the old current pos
  cp1 = ISRCounter;

  if (signbit(cp1) == signbit(cp0))
  {
    dcp = (cp1 - cp0);// in [ct]
  }
  else
  {
    dcp = (cp1 + cp0);// in [ct]
  }

  //dcp=(cp1 - cp0);
  //vel in rad/s
  //return double((dcp/dt) * 1000000.0 * enc2rad);
  return double((dcp/dts) * enc2rad);

}


int main()
{
    if(wiringPiSetup()<0){
        cout<<"setup wiring pi failed"<<endl;
        return 1;
    }
    if(wiringPiISR (channelPinA, INT_EDGE_BOTH, &doEncodeA) < 0 ) {
       cout<< "Unable to setup ISR"<<endl;;
      return 1;
    }
    if(wiringPiISR (channelPinB, INT_EDGE_BOTH, &doEncodeB) < 0 ) {
       cout<< "Unable to setup ISR"<<endl;;
      return 1;
    }

    Actuator_tb6612fng m1(23, 24, 25, 22);
    m1.Enable();

    double tgv = 0;
    double interval=5;
    //Cv.AntiWindup(-1023,1023);
    IPlot pVt(dts,"Vel vs time ", "Time (s)", "Velocity signal (rad/s) ");
    IPlot cs_vel(dts,"csv vs time ", "Time (s)", "Velocity control signal (pwm) ");
    IPlot err_vel(dts,"error vs time ", "Time (s)", "Velocity control error ");

    for (double i=0;i<interval; i+=dts){
        tgv = 3;
        //Velocity loop control signal computation
        v1 = getVel();
        esv = tgv - v1;
        csv = esv > Cv;
        //csv = esv > Fcv;
        m1.SetThrottle(csv);
        pVt.pushBack(getVel());
        //pVt.pushBack(getPos());
        cs_vel.pushBack(csv);
        err_vel.pushBack(esv);
        ts.WaitSamplingTime();
    }
    m1.SetThrottle(0);
    m1.Disable();
    pVt.Plot();
    cs_vel.Plot();
    err_vel.Plot();
return 0;
}



