#include "wiringPi.h"

//class PIDBlock
//{
//  public:
//    PIDBlock(double kp, double ki, double kd, double new_Ts);

//    double OutputUpdate(double input);

//    friend double operator > (double input, PIDBlock& output)
//    {
//      return output.OutputUpdate(input);
//    }

//    double GetState() const;


//  private:

//    double kp, ki, kd, Ts;
//    double cp, ci, cd;

//    double state;
//    double lastInput, sumInput;
//    long Initial(double new_Ts);
//};


class QuadEncoder
{
  public:
    QuadEncoder(int new_pinA = 2, int new_pinB = 3);

  private:
    int pinA;
    int pinB;

    static long counter; //TODO: make this variable not static for different instances.
    static long ClassInstances; //Need to prevent more than one instance.


    static void flankA();
    static void flankB();

};

class Actuator_tb6612fng
{
  public:
    Actuator_tb6612fng(int new_pinPWMA = 23, int new_pinAIN1 = 24, int new_pinAIN2 = 25, int new_pinSTBY = 22);
    int SetThrottle(int throttle); //-255 to 255
    int Enable();
    int Disable();

  private:
    int pinPWMA;
    int pinAIN1;
    int pinAIN2;
    int pinSTBY;
};

//TODO: use the proper files (cpp for implementation)
//#include "servo.h"


//PIDBlock::PIDBlock(double new_kp = 1, double new_ki = 0, double new_kd = 0, double new_Ts = 0.01)
//{
//  Ts = new_Ts;

//  kp = new_kp;
//  ki = new_ki;
//  kd = new_kd;

//}


//double PIDBlock::OutputUpdate(double input)
//{

// // if (isnan(input)) return 0;
  
////  sumInput += input;
////  ci = ki * Ts * sumInput;

//  cp = input * kp;
//  ci += input * ki * Ts;
//  cd = kd * 2;

//      if (abs(ci)>255)
//      {
//        ci=255;
//  
//      }  

// reset unwinder
//      if (signbit(ci)!=signbit(input))
//      {
//        sumInput=0;
//        ci=0;
//  
//      }

  /*state = cp + ci + cd;
  
//    Serial.print(ci);
//    Serial.print('\n');
  lastInput = input;

  return state;
}

double PIDBlock::GetState() const
{
  return state;
}

/*Quadrature Encoder Class*/





Actuator_tb6612fng::Actuator_tb6612fng(int new_pinPWMA, int new_pinAIN1, int new_pinAIN2, int new_pinSTBY)
{
  pinPWMA = new_pinPWMA;
  pinAIN1 = new_pinAIN1;
  pinAIN2 = new_pinAIN2;
  pinSTBY = new_pinSTBY;

  pinMode(pinAIN2, OUTPUT);
  pinMode(pinAIN1, OUTPUT);
  pinMode(pinPWMA, PWM_OUTPUT);
  pinMode(pinSTBY, OUTPUT);

  Enable();

}



//Funciones que controlan el motor
int Actuator_tb6612fng::SetThrottle(int throttle) //-255 to 255
{
  //      Serial.print("throttle");
  //    Serial.print('\n');
//for (int i=1;i<10;i++)
//{
//  delayMicroseconds(1);
//}

  if (throttle > 0)
  {
    digitalWrite(pinAIN1, HIGH);
    digitalWrite(pinAIN2, LOW);
    if (throttle > 1023)
    {
      pwmWrite(pinPWMA, 1023);
      
//      Serial.print("Saturation!!\n");
      return +2;
    }
    else
    {
      pwmWrite(pinPWMA, throttle);
      return +1;
    }
  }

  if (throttle < 0)
  {
    digitalWrite(pinAIN1, LOW);
    digitalWrite(pinAIN2, HIGH);
    if (throttle < -1023)
    {
      pwmWrite(pinPWMA, 1023);
//      Serial.print("Saturation!!\n");
      return -2;
    }
    else
    {
      pwmWrite(pinPWMA, -throttle);
//      Serial.print(-throttle);
//      Serial.print('\n');
      return -1;
    }
  }


  //else, it is zero, use Short brake mode
  digitalWrite(pinAIN1, HIGH);
  digitalWrite(pinAIN2, HIGH);
  pwmWrite(pinPWMA, 0);

  return 0;

}


int Actuator_tb6612fng::Enable()
{
  digitalWrite(pinSTBY, HIGH);
  return 0;
}

int Actuator_tb6612fng::Disable()
{
  digitalWrite(pinSTBY, LOW);
  return 0;
}
