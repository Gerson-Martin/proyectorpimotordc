#include <iostream>		// Include all needed libraries here
#include <fstream>
#include <wiringPi.h>
#include <cmath>
#include "servoControl.h"
#include "FPDBlock.h"
#include "fcontrol.h"
#include "IPlot.h"

using namespace std;	
int dutty;
//encoder
#define channelPinA  10
#define channelPinB  11
int counter0 = 0, counter1 = 0;
int contEnA, contEnB;
volatile int ISRCounter = 0;
double enc2rad = 2.0 * 3.14159 / 5880.0; //28*210=5880 pulse/rev.

//control
double t0 = 0, t1 = 0, dt = 0;
int cp0 = 0, cp1 = 0, dcp = 0;
double v0 = 0, v1 = 0, p0 = 0, p1 = 0;
double dts = 0.004; //seconds
SamplingTime ts(dts);

//control variables
//PIDBlock Cv(29.926, 1121.6, 0, dts);
PIDBlock Cv(163.9, 4462.3, 0, dts);
double csv = 0, esv = 0;

PIDBlock Cp(2.0, 0.0, 0, dts);

//FPDBlock Fcv(57.2010, 1921, -0.96, dts);
FPDBlock Fcv(323.98, 7388.5, -0.91, dts);


double csp = 0, esp = 0;
double t = 0;

void doEncodeA()
{
  contEnA ++;
  if (digitalRead(channelPinA) == digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
}
void doEncodeB() {
  contEnB ++;
  if (digitalRead(channelPinA) != digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
}
double getPos()
{
  //pos in rad
  return double(ISRCounter) * enc2rad;
  
}
double getVel()
{

  t0 = t1; //last time is the old current time
  t1 = micros();
  dt = (t1 - t0); //in [us];
  cp0 = cp1; //last pos is the old current pos
  cp1 = ISRCounter;
  if (signbit(cp1) == signbit(cp0))
  {
    dcp = (cp1 - cp0);// in [ct]
  }
  else
  {
    dcp = (cp1 + cp0);// in [ct]
  }

  //vel in rad/s
  //return double((dcp/dt) * 1000000.0 * enc2rad);
  return double((dcp/dts) * enc2rad);
  

}


int main()
{
    std::ofstream myFile("datos.csv");
    ofstream data("/home/pi/proyectos/c_test/data/pos_20__frac.csv",std::ofstream::out);
	if(wiringPiSetup()<0){
		cout<<"setup wiring pi failed"<<endl;
		return 1;
	}
	if(wiringPiISR (channelPinA, INT_EDGE_BOTH, &doEncodeA) < 0 ) {
       cout<< "Unable to setup ISR"<<endl;;
      return 1;
    }
    if(wiringPiISR (channelPinB, INT_EDGE_BOTH, &doEncodeB) < 0 ) {
       cout<< "Unable to setup ISR"<<endl;;
      return 1;
    }
  
	Actuator_tb6612fng m1(23, 24, 25, 22);
	m1.Enable(); 
	
	double tgv = 0.0;
    double tgp = 4.0;
    double interval=8;
    Cv.AntiWindup(-1023,1023);

    IPlot pVt(dts,"Position vs time ", "Time (s)", "Position signal (rad) ");
    //IPlot err(dts,"error vs time ", "Time (s)", "error (rad) ");
    //IPlot cs_p(dts,"csp vs time ", "Time (s)", "control singnal position  ");

    for (double i=0;i<interval; i+=dts){
	  //Position loop control signal computation
        p1 = getPos();
        esp = tgp - p1;
        csp = esp > Cp;
        //Target of lower loop is high loop control signal.
        tgv = csp;
        //Velocity loop control signal computation
        v1 = getVel();
        esv = tgv - v1;
        csv = esv > Cv;
        //csv = esv > Fcv;
        m1.SetThrottle(csv);
        //delayMicroseconds(825000*dts - 100*(contEnA+contEnB) ); //dts in seconds, and delay microseconds
        //cout <<t<<" pos:"<<getPos()<<" vel:"<<v1<<" csv:"<<csv<<" esv:"<<esv<<endl;
        data <<t<< " , " <<getPos() << " , " << tgp << " , " << csp << " , " << csv << endl;
        pVt.pushBack(getPos());
        //err.pushBack(esp);
        //cs_p.pushBack(csp);
        ts.WaitSamplingTime();
    }
    //go to zero
    for (double i=0;i<interval; i+=dts){
      //Position loop control signal computation
        p1 = getPos();
        esp = 0- p1;
        csp = esp > Cp;
        //Target of lower loop is high loop control signal.
        tgv = csp;
        //Velocity loop control signal computation
        v1 = getVel();
        esv = tgv - v1;
        csv = esv > Cv;
        //csv = esv > Fcv;
        m1.SetThrottle(csv);

        ts.WaitSamplingTime();
    }
    cout<<"hola"<<endl;
    ts.WaitSamplingTime();
    m1.SetThrottle(0);
    m1.Disable();
    data.close();
    pVt.Plot();
	
return 0;
}



