#include <iostream>		// Include all needed libraries here
#include <fstream>
#include <wiringPi.h>
#include "servoControl.h"
#include "FPDBlock.h"
#include "fcontrol.h"

using namespace std;
int dutty;
//encoder
#define channelPinA  13
#define channelPinB  14
volatile int ISRCounter = 0,n_pulseA=0,n_pulseB=0;
double enc2rad = 2.0 * 3.14159 / 5880.0; //28*210=5880 pulse/rev. [enc2rad]=[rad/pulse]
int cp0 = 0, cp1 = 0, dcp = 0;
double dts = 0.004; //seconds
SamplingTime ts(dts);

void doEncodeA()
{
 n_pulseA ++;
  if (digitalRead(channelPinA) == digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
 cout<<"A:"<<digitalRead(channelPinA)<<";"<<digitalRead(channelPinB)<<"::"<<ISRCounter<<endl;
}
void doEncodeB() {
   n_pulseB ++;
  if (digitalRead(channelPinA) != digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
   cout<<"B:"<<digitalRead(channelPinA)<<";"<<digitalRead(channelPinB)<<"::"<<ISRCounter<<endl;
}
double getPos()
{
  //pos in rad
  return double(ISRCounter) * enc2rad;
}
double getVel()
{
  cp0 = cp1; //last pos is the old current pos
  cp1 = ISRCounter;
  if (signbit(cp1) == signbit(cp0))
  {
    dcp = (cp1 - cp0);// in [ct]
  }
  else
  {
    dcp = (cp1 + cp0);// in [ct]
  }

  //vel in rad/s

  return double((dcp/dts) * enc2rad);

}
int main()
{
        ofstream data("/home/pi/proyectos/c_test/data/vel_data_1023.csv",std::ofstream::out);

        if(wiringPiSetup()<0){
           cout<<"setup wiring pi failed"<<endl;
           return 1;
        }
        if(wiringPiISR (channelPinA, INT_EDGE_BOTH, &doEncodeA) < 0 ) {
           cout<< "Unable to setup ISR"<<endl;;
           return 1;
        }
        if(wiringPiISR (channelPinB, INT_EDGE_BOTH, &doEncodeB) < 0 ) {
           cout<< "Unable to setup ISR"<<endl;;
           return 1;
        }

        Actuator_tb6612fng m1(23, 24, 25, 22);
        m1.Enable();
        m1.SetThrottle(-300);
        for(int i=0;ISRCounter<100;i++){
             ts.WaitSamplingTime();
        }
         m1.SetThrottle(0);
         m1.Disable();
         data.close();
         return 0;
}
