#include <iostream>		// Include all needed libraries here
#include <fstream>
#include <wiringPi.h>

using namespace std;		// No need to keep using “std”
unsigned int prevTime;
unsigned int dutty;
int ledPin = 23;
int main()
{
	std::ofstream myFile("data.csv");
	
	if(wiringPiSetup()<0){
		cout<<"setup wiring pi failed"<<endl;
		return 1;
	}
pinMode(ledPin, PWM_OUTPUT);		// Configure GPIO1 as an output for PWM

// Main program loop
while(1)
{
	//dutty=1000;
	//pwmWrite(ledPin, dutty);		
	//cout<<"pwm"<<endl;
	//delay(1);


	for(dutty= 0; dutty < 1023; dutty=dutty+20)
	{
		pwmWrite(ledPin, dutty);		
		cout<<"dutty:"<<dutty<<endl;
		myFile << "Controller PID" << " , " << dutty << endl;
		delay(100);
	}	
	
	delay(200);

	/*
		for(int i = 1023; i >= 0; i--)
	{
		pwmWrite(ledPin, i);		
		cout<<"i:"<<i<<endl;
		delay(100);
	}	
	*/
	
}
 // Close the file
    myFile.close();
return 0;
}
