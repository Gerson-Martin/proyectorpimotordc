#include <iostream>		// Include all needed libraries here
#include <fstream>
#include <wiringPi.h>
#include "servoControl.h"
#include "FPDBlock.h"
#include "fcontrol.h"

using namespace std;
int dutty;
//encoder
#define channelPinA  10
#define channelPinB  11
volatile int ISRCounter = 0,n_pulseA=0,n_pulseB=0;
double enc2rad = 2.0 * 3.14159 / 5880.0; //28*210=5880 pulse/rev. [enc2rad]=[rad/pulse]
int cp0 = 0, cp1 = 0, dcp = 0;
double dts = 0.004; //seconds
SamplingTime ts(dts);

void doEncodeA()
{
 n_pulseA ++;
   cout<<"A:"<<digitalRead(channelPinA)<<";"<<digitalRead(channelPinB)<<"::";
  if (digitalRead(channelPinA) == digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
  cout<<ISRCounter<<endl;
}
void doEncodeB() {
   n_pulseB ++;
   cout<<"B:"<<digitalRead(channelPinA)<<";"<<digitalRead(channelPinB)<<"::";
  if (digitalRead(channelPinA) != digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
   cout<<ISRCounter<<endl;
  //cout<<n_pulse<<endl;
}
double getPos()
{
  //pos in rad
  return double(ISRCounter) * enc2rad;
  //return double(ISRCounter);
}
double getVel()
{
  cp0 = cp1; //last pos is the old current pos
  cp1 = ISRCounter;
  if (signbit(cp1) == signbit(cp0))
  {
    dcp = (cp1 - cp0);// in [ct]
  }
  else
  {
    dcp = (cp1 + cp0);// in [ct]
  }

  //vel in rad/s

  return double((dcp/dts) * enc2rad);

}
int main()
{
        ofstream data("/home/pi/proyectos/c_test/data/vel_data_1023.csv",std::ofstream::out);

        if(wiringPiSetup()<0){
           cout<<"setup wiring pi failed"<<endl;
           return 1;
        }
        if(wiringPiISR (channelPinA, INT_EDGE_BOTH, &doEncodeA) < 0 ) {
           cout<< "Unable to setup ISR"<<endl;;
           return 1;
        }
        if(wiringPiISR (channelPinB, INT_EDGE_BOTH, &doEncodeB) < 0 ) {
           cout<< "Unable to setup ISR"<<endl;;
           return 1;
        }

        Actuator_tb6612fng m1(23, 24, 25, 22);
        m1.Enable(); 

        float target=0;
        float err_pos=0;
        target=-6.28;
        m1.SetThrottle(-1023);
        for(int i=0;ISRCounter<100;i++){
             ts.WaitSamplingTime();
           // cout<<ISRCounter<<endl;//";A:"<<n_pulseA<<";b:"<<n_pulseB<<";   ";
        }
       // while(ISRCounte){
            /*err_pos=target-getPos();
            if(signbit(err_pos))// 1 is equal a negative number
            {
              m1.SetThrottle(400);
            }else {              //0 is equal a number positive
             m1.SetThrottle(-400);
            }
            if (abs(err_pos)<0.01)
                break;
            ts.WaitSamplingTime();
            cout<<"Posicion:"<<getPos()<<endl;*/

         //}

                /*float target=0;
                float err_vel=0;
                float err_pos=0;
                target=-6.28;
                float target_vel=1;
                float deltatime=0;
                while(1){
                    err_vel=target_vel-getVel();

                    if(signbit(err_pos))// 1 is equal a negative number
                    {
                      m1.SetThrottle(400);
                    }else {              //0 is equal a number positive
                     m1.SetThrottle(-400);
                    }
                    if (abs(err_pos)<0.01)
                        break;
                    ts.WaitSamplingTime();
                    cout<<"Posicion:"<<getPos()<<endl;
                 }*/


         m1.SetThrottle(0);
         m1.Disable();
         data.close();
         return 0;
}


