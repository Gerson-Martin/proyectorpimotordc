#include <iostream>		// Include all needed libraries here
#include <fstream>
#include <wiringPi.h>
#include "servoControl.h"

using namespace std;	
int dutty;
//encoder
#define channelPinA  27
#define channelPinB  28
int counter0 = 0, counter1 = 0;
int contEnA, contEnB;
volatile int ISRCounter = 0;
double enc2rad = 2.0 * 3.14159 / 5880.0; //28*210=5880 pulse/rev.

void doEncodeA()
{
  contEnA ++;
  if (digitalRead(channelPinA) == digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
}
void doEncodeB() {
  contEnB ++;
  if (digitalRead(channelPinA) != digitalRead(channelPinB))
    ISRCounter ++;
  else
    ISRCounter --;
}
double getPos()
{
  //pos in rad
  return double(ISRCounter) * enc2rad;
  //return double(ISRCounter);
}
int main()
{
	std::ofstream myFile("data.csv");
	if(wiringPiSetup()<0){
		cout<<"setup wiring pi failed"<<endl;
		return 1;
	}
	if(wiringPiISR (channelPinA, INT_EDGE_BOTH, &doEncodeA) < 0 ) {
       cout<< "Unable to setup ISR"<<endl;;
      return 1;
    }
    if(wiringPiISR (channelPinB, INT_EDGE_BOTH, &doEncodeB) < 0 ) {
       cout<< "Unable to setup ISR"<<endl;;
      return 1;
    }
    
	Actuator_tb6612fng m1(23, 24, 25, 22);
	m1.Enable(); 
	
while(1)
{
	while(getPos()<6.3)
	{
		m1.SetThrottle(200);			
		cout<<"pos:"<<getPos()<<endl;
		delay(200);
	}
	/*
	for(dutty= 0; dutty < 1000; dutty=dutty+20)
	{
		m1.SetThrottle(dutty);			
		cout<<"pos:"<<getPos()<<endl;
		//myFile << "Controller PID" << " , " << dutty << endl;
		delay(200);
	}	
	*/
	delay(200);
	//m1.Disable(); 
/*	
	for(dutty= 0; dutty < 1000; dutty=dutty+20)
	{
		m1.SetThrottle(-dutty);	
		cout<<"pos:"<<ISRCounter<<endl;
		myFile << "Controller PID" << " , " << dutty << endl;
		delay(100);
	}
	delay(200);
	*/
}
    myFile.close();
return 0;
}


